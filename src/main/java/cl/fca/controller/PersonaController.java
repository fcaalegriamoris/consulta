package cl.fca.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.fca.models.Persona;
import cl.fca.service.IPersonaService;

@RestController
@RequestMapping("/personas")
public class PersonaController {

	IPersonaService service;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Persona>> listar(){
		List<Persona> personas = new ArrayList<>();
		personas = service.getAll();		
		return new ResponseEntity<List<Persona>>(personas, HttpStatus.OK);		
		
	}
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public Persona registrar(@RequestBody Persona m) {
		return service.persist(m);
	}
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Persona> listarId(@PathVariable("id") Integer id){		
		Persona p = service.findById(id);		
		return new ResponseEntity<Persona>(p, HttpStatus.OK);		
		
	}
	@GetMapping(value="/buscar-rut/{run}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Persona> listarIdentificadorSocial(@PathVariable("run") String run){		
		Persona p = service.findByRun(run);		
		return new ResponseEntity<Persona>(p, HttpStatus.OK);		
		
	}
	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object>actualizar(@Valid @RequestBody Persona p){
		service.merge(p);
		return new ResponseEntity<Object>(HttpStatus.OK);
		
	}
	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> eliminar(@PathVariable Integer id) {
		Persona p = service.findById(id);
		if(p == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}else{
			try {
				service.delete(id);
				return new ResponseEntity<Object>(HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
}
