package cl.fca.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.fca.models.Especialidad;
import cl.fca.service.IEspecialidadService;

@RestController
@RequestMapping("/especialidades")
public class EspecialidadController {
	@Autowired
	IEspecialidadService service;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Especialidad>> listar(){
		List<Especialidad> especialidades = new ArrayList<>();
		especialidades = service.getAll();
		return new ResponseEntity<List<Especialidad>>(especialidades,  HttpStatus.OK);
	}
	
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Especialidad registrar(@RequestBody Especialidad e) {
		return service.persist(e);
	}
	
	@GetMapping(value="/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Especialidad> listarId(@PathVariable("id") Integer id){
		Especialidad e  = service.findById(id);
		return new ResponseEntity<Especialidad>(e, HttpStatus.OK);		
		
	}
	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object>actualizar(@Valid @RequestBody Especialidad especialidad){
		service.merge(especialidad);
		return new ResponseEntity<Object>(HttpStatus.OK);
		
	}
	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> eliminar(@PathVariable Integer id) {
		Especialidad e = service.findById(id);
		if(e == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}else{
			try {
				service.delete(id);
				return new ResponseEntity<Object>(HttpStatus.OK);
			} catch (Exception ex) {
				return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
}
