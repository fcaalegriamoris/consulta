package cl.fca.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="detalle_consulta")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) // correccion error con serialize
public class DetalleConsulta {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String sintoma;
	private String diagnostico;
	private String tratamiento;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="id_consulta",nullable=false)
	private Consulta consulta;
	
	@OneToMany(
			mappedBy = "detalleConsulta", // nombre de la variable en la otra entidad
			cascade = {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REMOVE},
			fetch = FetchType.LAZY,orphanRemoval=true
	)
	private List<Examen> examenes;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSintoma() {
		return sintoma;
	}
	public void setSintoma(String sintoma) {
		this.sintoma = sintoma;
	}
	public String getDiagnostico() {
		return diagnostico;
	}
	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}
	public String getTratamiento() {
		return tratamiento;
	}
	public void setTratamiento(String tratamiento) {
		this.tratamiento = tratamiento;
	}
	public Consulta getConsulta() {
		return consulta;
	}
	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}
	public List<Examen> getExamenes() {
		return examenes;
	}
	public void setExamenes(List<Examen> examenes) {
		this.examenes = examenes;
	}
}
