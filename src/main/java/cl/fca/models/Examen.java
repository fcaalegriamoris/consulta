package cl.fca.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="examen")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) // correccion error con serialize
public class Examen {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable=false, unique = true)
	private String codigo;
	@Column(columnDefinition = "int default 0")
	private Integer valor;
	
	private String categoria; // puede ser una tabla
	private String descripcion;
	private String requisitos; // puede ser una tabla con muchos a muchos
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="id_consulta",nullable=false)
	private DetalleConsulta detalleConsulta;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Integer getValor() {
		return valor;
	}
	public void setValor(Integer valor) {
		this.valor = valor;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getRequisitos() {
		return requisitos;
	}
	public void setRequisitos(String requisitos) {
		this.requisitos = requisitos;
	}
	public DetalleConsulta getDetalleConsulta() {
		return detalleConsulta;
	}
	public void setDetalleConsulta(DetalleConsulta detalleConsulta) {
		this.detalleConsulta = detalleConsulta;
	}
}
