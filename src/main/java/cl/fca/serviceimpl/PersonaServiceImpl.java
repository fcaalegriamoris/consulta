package cl.fca.serviceimpl;

import java.util.List;

import org.springframework.stereotype.Service;

import cl.fca.dao.IPersonaDAO;
import cl.fca.models.Persona;
import cl.fca.service.IPersonaService;

@Service
public class PersonaServiceImpl implements IPersonaService {

	IPersonaDAO service;

	@Override
	public Persona persist(Persona p) {
		return service.save(p);
	}

	@Override
	public List<Persona> getAll() {
		return service.findAll();
	}

	@Override
	public Persona findById(Integer id) {
		return service.findOne(id);
	}
	
	@Override
	public Persona findByRun(String run) {
		return service.buscarPorRun(run);
	}

	@Override
	public Persona merge(Persona p) {
		return service.save(p);
	}

	@Override
	public void delete(Integer id) {
		service.delete(id);
	}
}
