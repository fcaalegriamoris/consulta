package cl.fca.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.fca.dao.IMedicoDAO;
import cl.fca.models.Medico;
import cl.fca.service.IMedicoService;

@Service
public class MedicoServiceImpl implements IMedicoService {

	@Autowired
	IMedicoDAO service;
	
	@Override
	public Medico persist(Medico m) {
		return service.save(m);
	}

	@Override
	public List<Medico> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public Medico findById(Integer id) {
		// TODO Auto-generated method stub
		return service.getOne(id);
	}

	@Override
	public Medico merge(Medico m) {
		// TODO Auto-generated method stub
		return service.save(m);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		service.delete(id);
	}

}
