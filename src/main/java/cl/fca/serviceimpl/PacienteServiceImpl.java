package cl.fca.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import cl.fca.dao.IPacienteDAO;
import cl.fca.models.Paciente;
import cl.fca.service.IPacienteService;

public class PacienteServiceImpl implements IPacienteService {

	@Autowired
	IPacienteDAO service;
	
	@Override
	public Paciente persist(Paciente p) {
		// TODO Auto-generated method stub
		return service.save(p);
	}

	@Override
	public List<Paciente> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public Paciente findById(Integer id) {
		// TODO Auto-generated method stub
		return service.getOne(id);
	}

	@Override
	public Paciente merge(Paciente p) {
		// TODO Auto-generated method stub
		return service.save(p);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		service.delete(id);
	}
}
