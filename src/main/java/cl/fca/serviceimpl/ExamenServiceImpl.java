package cl.fca.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import cl.fca.dao.IExamenDAO;
import cl.fca.models.Examen;
import cl.fca.service.IExamenService;

public class ExamenServiceImpl implements IExamenService {

	@Autowired
	IExamenDAO service;
	
	@Override
	public Examen persist(Examen e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public List<Examen> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public Examen findById(Integer id) {
		// TODO Auto-generated method stub
		return service.getOne(id);
	}

	@Override
	public Examen merge(Examen e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		service.delete(id);
	}
}
