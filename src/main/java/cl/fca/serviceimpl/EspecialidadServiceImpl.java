package cl.fca.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.fca.dao.IEspecialidadDAO;
import cl.fca.models.Especialidad;
import cl.fca.service.IEspecialidadService;

@Service
public class EspecialidadServiceImpl implements IEspecialidadService {
	
	@Autowired
	IEspecialidadDAO service;
	
	@Override
	public Especialidad persist(Especialidad e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public List<Especialidad> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public Especialidad findById(Integer id) {
		// TODO Auto-generated method stub
		return service.getOne(id);
	}

	@Override
	public Especialidad merge(Especialidad e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		service.delete(id);
	}

}
