package cl.fca.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.fca.dao.IConsultaDAO;
import cl.fca.models.Consulta;
import cl.fca.service.IConsultaService;

@Service
public class ConsultaServiceImpl implements IConsultaService {
	
	@Autowired
	IConsultaDAO service;
	
	@Override
	public Consulta persist(Consulta c) {
		// TODO Auto-generated method stub
		c.getDetalles().forEach( x->x.setConsulta(c) );
		Consulta tt= service.save(c);		
		return tt;
	}

	@Override
	public List<Consulta> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public Consulta findById(Integer id) {
		// TODO Auto-generated method stub
		return service.findOne(id);
	}

	@Override
	public Consulta merge(Consulta c) {
		// TODO Auto-generated method stub
		return service.save(c);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		service.delete(id);
	}

}
