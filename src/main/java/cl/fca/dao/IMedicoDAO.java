package cl.fca.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.fca.models.Medico;

@Repository
public interface IMedicoDAO extends JpaRepository<Medico, Integer>{

}
