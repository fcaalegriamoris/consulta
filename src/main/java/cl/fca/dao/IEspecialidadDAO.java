package cl.fca.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.fca.models.Especialidad;

@Repository
public interface IEspecialidadDAO extends JpaRepository<Especialidad, Integer>{

}