package cl.fca.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.fca.models.Consulta;

@Repository
public interface IConsultaDAO extends JpaRepository<Consulta, Integer>{

}
