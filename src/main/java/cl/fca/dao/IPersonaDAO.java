package cl.fca.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cl.fca.models.Persona;

@Repository
public interface IPersonaDAO extends JpaRepository<Persona, Integer> {
	@Query(value="select * from personas where run = :run", nativeQuery = true)
	Persona buscarPorRun(@Param("run") String run);
}
