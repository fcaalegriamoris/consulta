package cl.fca.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.fca.models.Examen;

@Repository
public interface IExamenDAO extends JpaRepository<Examen, Integer>{

}
