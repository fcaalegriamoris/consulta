package cl.fca.service;

import java.util.List;

import cl.fca.models.Consulta;

public interface IConsultaService {
	Consulta persist(Consulta e);
	List<Consulta> getAll();
	Consulta findById(Integer id);
	Consulta merge(Consulta e);
	void delete(Integer id);
}
