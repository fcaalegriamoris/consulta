package cl.fca.service;

import java.util.List;

import cl.fca.models.Persona;

public interface IPersonaService {
	Persona persist(Persona p);
	List<Persona> getAll();
	Persona findById(Integer id);
	Persona findByRun(String run);
	Persona merge(Persona p);
	void delete(Integer id);
}