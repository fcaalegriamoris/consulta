package cl.fca.service;

import java.util.List;

import cl.fca.models.Especialidad;

public interface IEspecialidadService {
	Especialidad persist(Especialidad e);
	List<Especialidad> getAll();
	Especialidad findById(Integer id);
	Especialidad merge(Especialidad e);
	void delete(Integer id);
}
