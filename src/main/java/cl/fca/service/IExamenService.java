package cl.fca.service;

import java.util.List;

import cl.fca.models.Examen;

public interface IExamenService {
	Examen persist(Examen e);
	List<Examen> getAll();
	Examen findById(Integer id);
	Examen merge(Examen e);
	void delete(Integer id);
}
