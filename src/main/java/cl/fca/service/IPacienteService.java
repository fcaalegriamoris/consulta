package cl.fca.service;

import java.util.List;

import cl.fca.models.Paciente;

public interface IPacienteService {
	Paciente persist(Paciente p);
	List<Paciente> getAll();
	Paciente findById(Integer id);
	Paciente merge(Paciente p);
	void delete(Integer id);
}
