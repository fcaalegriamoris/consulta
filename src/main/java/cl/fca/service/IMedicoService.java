package cl.fca.service;

import java.util.List;

import cl.fca.models.Medico;

public interface IMedicoService {
	Medico persist(Medico m);
	List<Medico> getAll();
	Medico findById(Integer id);
	Medico merge(Medico m);
	void delete(Integer id);
}
